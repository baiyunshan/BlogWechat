//app.js
const Towxml = require('/towxml/main');
App({
  Data: {
    userInfo: null,
    zaned: false
  },
  towxml: new Towxml(),
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
  },
  globalData: {
    userInfo: null,
    moveTIme:"2019-02-28"//系统开始运行的时间
  }
})